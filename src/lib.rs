/*
 * Copyright 2016 Alexander Schlarb
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*!
 * Sized function and method types
 * 
 * # Overview
 * 
 * The `sized_fn` crate offers types that store references to callback
 * functions without having to choose for your users on the trade-offs
 * involved. Please read the [Rationale](#Rationale) section, if you're
 * unsure why this is needed and how it is superior to other
 * "solutions".
 * 
 * # Limitations & Notes
 * 
 *  * When passing functions you need to manually coerce them using the `func as fn(_) -> _` syntax
 *  * There is currently a struct for every possible number of function parameters
 *    ([rust-lang/rfcs#376](https://github.com/rust-lang/rfcs/issues/376))
 *  * You need to use `callback.call(…)` to invoke the callback of a `SizedFn` type
 *    ([rust-lang/rfcs#1037](https://github.com/rust-lang/rfcs/issues/1037))
 *  * Closures and methods that modify their object/internal have to use the `SizedFnMut` variant, which must be stored
 *    as `mut` in order to be invokable
 * 
 * # Examples
 * 
 * ## Putting a function into a `SizedFn`, then calling it
 * 
 * ```
 * use sized_fn::*;
 * 
 * fn add_one(x: i32) -> i32 {
 * 	x + 1
 * }
 * 
 * fn main() { 
 * 	let callback = SizedFn1::from(add_one as fn(_) -> _);
 * 	assert_eq!(callback.call(5), 6);
 * }
 * ```
 * 
 * ## Call method that updates object state
 * 
 * ```
 * use sized_fn::*;
 * 
 * struct Divider {
 * 	divisor: i32,
 * 	result:  String
 * }
 * 
 * impl Divider {
 * 	pub fn new(divisor: i32) -> Self {
 * 		Divider {
 * 			divisor: divisor,
 * 			result:  String::new()
 * 		}
 * 	}
 * 
 * 	pub fn divide_and_format(&mut self, number: i32) {
 * 		self.result = format!("{} + {}/{}",
 * 			(number / self.divisor),
 * 			(number % self.divisor),
 * 			self.divisor
 * 		);
 * 	}
 * 	
 * 	pub fn get_result(&self) -> String {
 * 		self.result.clone()
 * 	}
 * }
 * 
 * fn main() { 
 * 	let mut divider = Divider::new(5);
 * 	
 * 	{
 * 		let mut callback = SizedFnMut1::from_mut(
 * 				(&mut divider, Divider::divide_and_format as fn(&mut _, _) -> _));
 * 		callback.call(29);
 * 	}
 * 	
 * 	assert_eq!(divider.get_result(), String::from("5 + 4/5"));
 * }
 * ```
 *
 * ## Passing a closure that updates values in the current function
 * 
 * ```
 * use sized_fn::*;
 * 
 * fn call_me(mut callback: SizedFnMut1<i32, ()>) {
 * 	callback.call(24);
 * }
 * 
 * fn main() {
 * 	let values = [1, 2, 3];
 * 	let mut result = 0;
 * 	{	
 * 		let mut closure = |num: i32| {
 * 			for value in values.iter() {
 * 				result += value * num;
 * 			}
 * 		};
 * 		
 * 		call_me(SizedFnMut1::from_mut(&mut closure));
 * 	}
 * 	
 * 	assert_eq!(result, 144);
 * }
 * ```
 * 
 * ## Create callback from method that returns struct value
 * 
 * ```
 * use sized_fn::*;
 * 
 * struct Divider {
 * 	divisor: i32
 * }
 * 
 * impl Divider {
 * 	pub fn new(divisor: i32) -> Self {
 * 		Divider {
 * 			divisor: divisor
 * 		}
 * 	}
 * 
 * 	pub fn divide_and_format(&self, number: i32) -> String {
 * 		format!("{} + {}/{}",
 * 			(number / self.divisor),
 * 			(number % self.divisor),
 * 			self.divisor
 * 		)
 * 	}
 * }
 * 
 * fn main() { 
 * 	let divider  = Divider::new(5);
 * 	let callback = SizedFn1::from(
 * 			(&divider, Divider::divide_and_format as fn(&_, _) -> _));
 * 	assert_eq!(callback.call(29), String::from("5 + 4/5"));
 * }
 * ```
 * 
 * ## Using immutable boxed closures
 *
 * ```
 * use sized_fn::*;
 * 
 * fn main() {
 * 	let values = [1, 2, 3];
 * 	let closure = move |num: i32| -> i32 {
 * 		let mut result = 0;
 * 		for value in values.iter() {
 * 			result += value * num;
 * 		}
 * 		
 * 		result
 * 	};
 * 	
 * 	let callback = SizedFn1::from(Box::new(closure));
 * 	assert_eq!(callback.call(24), 144);
 * }
 * ```
 * 
 * ## Using immutable closure references
 * 
 * ```
 * use sized_fn::*;
 * 
 * fn main() {
 * 	let values = [1, 2, 3];
 * 	let closure = |num: i32| -> i32 {
 * 		let mut result = 0;
 * 		for value in values.iter() {
 * 			result += value * num;
 * 		}
 * 		
 * 		result
 * 	};
 * 	
 * 	let callback = SizedFn1::from(&closure);
 * 	assert_eq!(callback.call(24), 144);
 * }
 * ```
 * 
 * # Rationale
 * 
 * Previously there were several possible ways to store a callback in a `struct`:
 *
 *  1. <u>Make the struct generic to the `Fn()` type of the callback</u>:
 * 
 *     ```
 *     struct Foo<F> where F: Fn(u32) -> u32 {
 * 	   	callback: F
 *     }
 *     ```
 * 
 *     This is the fastest possible approach (and NOT directly supported by
 *     this crate), but it unfortunately really lacks on the flexibility side
 *     of things…
 *
 *     *Pros*:
 *
 *      - **Very** fast (usually the function will be inlined)
 *      - Works with any function type
 *
 *     *Cons*:
 *
 *      - Compilation will **fail** if no callback is provided
 *      - You can never swap out the callback once its set
 * 
 *  2. <u>Store only the `Fn()` trait reference</u>:
 * 
 *     ```
 *     struct Foo<'a> {
 *     	callback: &'a Fn(u32) -> u32
 *     }
 *     ```
 * 
 *     Works almost always and is just a tiny bit slower then solution #1. The
 *     big drawback of this method is that the struct implementing the `Fn()`
 *     trait still needs to be stored *somewhere*. This can become a big burden
 *     for any `struct` that needs to survive for longer than the current stack
 *     frame (storing or, even worse, passing the struct to the calling
 *     function becomes something you'll probably think at least twice about).
 *     
 *     *Pros*:
 *     
 *      - Quite fast
 *      - Works on everything that solution *#1* works with
 *     
 *     *Cons*:
 *
 *      - `Fn()`'s `struct` needs to be stored *somewhere else*
 *      - The involved lifetimes are hard to get right (particularly for the
 *        caller)
 * 
 *  3. <u>Boxed closures</u>:
 *
 *     ```
 *     struct Foo {
 *     	callback: Box<Fn(u32) -> u32>
 *     }
 *     ```
 *     
 *     Always works, but is slow and requires a pretty pointless memory
 *     allocation. All the issues mentioned in *#2* disappear since the
 *     storage location is set to be that big, unstructured memory pool
 *     that is the heap.
 *
 *     *Pros*:
 *     
 *      - Easy, none of the storage problems of solution *#2*
 *      - Also works on everything that solution *#1* works with
 *
 *     *Cons*:
 *
 *      - The involved allocation is generally pretty unnecessary 
 *      - Not very fast (no cache locality)
 * 
 *  4. <u>Function pointers</u>:
 * 
 *     ```
 *     struct Foo {
 *     	callback: fn(u32) -> u32
 *     }
 *     ```
 * 
 *     "The good old C method.": As fast as solution #2 but with all the
 *     benefits of solution #3! The solution for everything? Not quite…
 *     
 *     Using this method makes it impossible to use anything but classic
 *     "C-like functions". No bound methods, no closures, just the good old
 *     stuff. This is particularly a problem (aside from not being very
 *     elegant), if you want to offer the called function any kind of private
 *     state (for methods that's typically their object instance).
 *     
 *     Some solutions that typically come to mind:
 *     
 *      1. Store an `Option<&T>` reference to some object, that is then passed
 *         the the callback:
 * 
 *         A nice and well tested solution, but (like solution #1) requires
 *         the `struct` to be generic with regards to `T` (although the
 *         entanglement is arguably a lot more manageable).
 *  
 *      2. Store an `Option<&Trait>` reference to some trait object, that is
 *         then passed to the callback:
 * 
 *         This is similar to the previous solution, but shifts the burden onto
 *         the callback and the given object.
 * 
 *      3. Store a raw pointer, integer or string, that is then passed to the
 *         callback:
 *
 *         Raw pointers are unsafe and tedious to use, integers and strings
 *         suck in most, if not all, scenarios.
 *     
 *     Also all of these "solutions" still exclude closures and bound methods
 *     and come with the elegance of your average 80's C-interface.
 *
 *     *Pros*:
 *
 *      - Also quite fast
 *      - No extra data to store (just the function pointer)
 * 
 *     *Cons*:
 *
 *      - Holding and passing around state for the callback is burdensome
 *      - Does not work with closures **at all**
 *      - Poor ergonomics
 * 
 * There must be a better way, right? Something that combines all of the above
 * solutions into something that works great, whatever the circumstances?
 * 
 * That's were this crate comes in: It provides a type-safe and easy-to-use way
 * to store "something callbackey" and then let the user choose which trade-offs
 * *they* want to make.
 * 
 * *Pros*:
 *
 *  - Works with any function type (like solution #1)
 *  - Has native support for bound methods (by storing a reference to the
 *    target object)
 *  - Almost as fast as solution #4
 *  - Lets the user choose which trade-off they want to make
 * 
 * *Cons*:
 * 
 *  - Slightly slower than solution #2 and #4
 *  - Uses more stack memory than just storing a simple function pointer
 *
 * Basically `sized_fn` is the dynamic version of solution #1!
 */

#![warn(missing_docs)]


/**
 * Similar to [std::convert::From](https://doc.rust-lang.org/std/convert/trait.From.html), but for constructors
 * that only apply to the `SizedFnMut` variant of the struct
 */
pub trait FromMut<T>: Sized {
    /// Performs the conversion.
    fn from_mut(T) -> Self;
}



/**
 * Generate a structure for storing a method and its object
 *
 * # Examples
 * 
 * ```
 * sized_fn_method_struct!(const SizedFnMethod2(A, B));
 * sized_fn_method_struct!(mut   SizedFnMutMethod2(A, B));
 * ```
 */
macro_rules! sized_fn_method_struct {
	( const $name:ident [ $($t:ident),* ] ) => (
		sized_fn_method_struct!(__ $name ($($t),*) + ());
	);
	
	( mut   $name:ident [ $($t:ident),* ] ) => (
		sized_fn_method_struct!(__ $name ($($t),*) + (mut));
	);
	
	( __ $name:ident ( $($t:ident),* ) + ($($attrib:ident)*) ) => (
		#[doc(hidden)] // This is an implementation detail
		pub struct $name<'a $(, $t)*, Z> {
			object: &'a usize,
			method: fn(&'a usize $(, $t)*) -> Z,
		}
		
		#[doc(hidden)]
		impl<'a $(, $t)*, Z> $name<'a $(, $t)*, Z> {
			pub fn new<T>(object: &'a $($attrib)* T, method: fn(& $($attrib)* T $(, $t)*) -> Z) -> $name<'a $(, $t)*, Z> {
				use std::mem;
				
				unsafe {
					// Since the given type T is never actually dereferenced
					// (only passed around), it's "mostly safe" to store it as
					// pointing to the wrong type (and thereby avoiding the
					// type parameter)
					// When the actual target method is called, it *will* receive
					// the reference with the wrong type value. The method will not
					// notice this however, since (on the machine-code level) we'll
					// actually just pass it a raw pointer that will then be
					// interpreted like the original type.
					$name {
						object: mem::transmute(object),
						method: mem::transmute(method),
					}
				}
			}
			
			/**
			 * Call this to invoke wrapped callback function
			 * 
			 * This is a workaround until the `Fn()`-trait is stable
			 * ([rust-lang/rfcs#1037](https://github.com/rust-lang/rfcs/issues/1037)).
			 */
			#[inline]
			#[allow(non_snake_case)] // Lower-case parameter names would require extra identifiers
			pub fn call(&self $(, $t: $t)*) -> Z {
				let method = self.method;
				method(self.object, $($t,)*)
			}
		}
	);
}



macro_rules! sized_fn_struct {
	( _comm $name:ident [ $($t:ident),* ] with $name_method:ident ) => (
		/**
		 * Create `SizedFn` from bare function reference
		 * 
		 * A SizedFn created like this works almost the same as passing a
		 * `fn(…) -> …` reference directly to another function:<br />
		 * The reference to the function will be stored and later invoked
		 * upon `call()`.
		 *
		 * # Examples
		 *
		 * See the [Putting a function into a `SizedFn`, then calling it](index.html#putting-a-function-into-a-sized_fn-then-calling-it) example.
		 */
		impl<'a, $($t,)* Ret> From<fn($($t),*) -> Ret> for $name<'a, $($t,)* Ret> {
			fn from(function: fn($($t,)*) -> Ret) -> Self {
				$name::Function(function)
			}
		}
		
		/**
		 * Create `SizedFn` from object and bare method function reference
		 * 
		 * This is the most important use-case of `SizedFn`!<br />
		 * Pass around and store references to bound methods without:
		 * 
		 *   * …having to use type parameters on traits.
		 *   * …having to store that referenced `Fn()` trait object *somewere else* (because it isn't `Sized`).
		 *   * …having to heap-allocate anything.
		 * 
		 * # Examples
		 * 
		 * See the [Create callback from method that returns struct value](index.html#create-callback-from-method-that-returns-struct-value) example.
		 */
		impl<'a, $($t,)* Ret, Type> From<(&'a Type, fn(&Type, $($t),*) -> Ret)> for $name<'a, $($t,)* Ret> {
			fn from(method_bundle: (&'a Type, fn(&Type, $($t),*) -> Ret)) -> Self {
				let (object, method) = method_bundle;
				$name::Method($name_method::new(object, method))
			}
		}
		
		/**
		 * Create `SizedFn` from a closure reference
		 * 
		 * # Examples
		 * 
		 * See the [Using immutable closure references](index.html#using-immutable-closure-references) example.
		 */
		impl<'a, $($t,)* Ret, Fun> From<&'a Fun> for $name<'a, $($t,)* Ret> where Fun: Fn($($t),*) -> Ret {
			fn from(closure: &'a Fun) -> Self {
				$name::FnRef(closure)
			}
		}
		
		/**
		 * Create `SizedFn` from a boxed closure
		 * 
		 * # Examples
		 * 
		 * See the [Using immutable boxed closures](index.html#using-immutable-boxed-closures) example.
		 */
		impl<'a, $($t,)* Ret, Fun> From<Box<Fun>> for $name<'a, $($t,)* Ret> where Fun: Fn($($t,)*) -> Ret + 'static {
			fn from(closure: Box<Fun>) -> Self {
				$name::FnBox(closure)
			}
		}
	);
	
	( const $name:ident [ $($t:ident),* ] with $name_method:ident ) => (
		#[allow(missing_docs)] // All enum variants are documented in the respective constructors
		pub enum $name<'a, $($t: 'a,)* Z: 'a> {
			Function(fn($($t,)*) -> Z),
			Method($name_method<'a, $($t,)* Z>),
			FnRef(&'a Fn($($t,)*) -> Z),
			FnBox(Box<Fn($($t,)*) -> Z>),
		}
		
		impl<'a, $($t,)* Z> $name<'a, $($t,)* Z> {
			/**
			 * Call this to invoke wrapped callback function
			 * 
			 * This is a workaround until the `Fn()`-trait is stable
			 * ([rust-lang/rfcs#1037](https://github.com/rust-lang/rfcs/issues/1037)).
			 */
			#[inline]
			#[allow(non_snake_case)] // Lower-case parameter names would require extra identifiers
			pub fn call(&self $(, $t: $t)*) -> Z {
				match self {
					&$name::Function(ref function) => {
						function($($t),*)
					},
					
					&$name::Method(ref method)     => {
						method.call($($t),*)
					},
					
					&$name::FnRef(ref closure) => {
						closure($($t),*)
					},
					
					&$name::FnBox(ref closure) => {
						closure($($t),*)
					},
				}
			}
		}
		
		sized_fn_struct!(_comm $name [ $($t),* ] with $name_method);
	);
	
	
	
	
	( mut $name:ident [ $($t:ident),* ] from $name_const:ident with $name_method:ident + $name_method_mut:ident ) => (
		#[allow(missing_docs)] // All enum variants are documented in the respective constructors
		pub enum $name<'a, $($t: 'a,)* Z: 'a> {
			Function(fn($($t,)*) -> Z),
			Method($name_method<'a, $($t,)* Z>),
			FnRef(&'a Fn($($t,)*) -> Z),
			FnBox(Box<Fn($($t,)*) -> Z>),
			
			/* Added section */
			MethodMut($name_method_mut<'a, $($t,)* Z>),
			FnMutRef(&'a mut FnMut($($t),*) -> Z),
			FnMutBox(Box<FnMut($($t),*) -> Z>),
		}
		
		impl<'a, $($t,)* Z> $name<'a, $($t,)* Z> {
			/**
			 * Call this to invoke wrapped callback function
			 * 
			 * This is a workaround until the `Fn()`-trait is stable
			 * ([rust-lang/rfcs#1037](https://github.com/rust-lang/rfcs/issues/1037)).
			 */
			#[inline]
			#[allow(non_snake_case)] // Lower-case parameter names would require extra identifiers
			pub fn call(&mut self, $($t: $t),*) -> Z {
				match self {
					&mut $name::Function(ref function) => {
						function($($t),*)
					},
					
					&mut $name::Method(ref method)     => {
						method.call($($t),*)
					},
					
					&mut $name::FnRef(ref closure) => {
						closure($($t),*)
					},
					
					&mut $name::FnBox(ref closure) => {
						closure($($t),*)
					},
					
					
					/* Added section */
					&mut $name::MethodMut(ref mut method) => {
						method.call($($t),*)
					},
					
					&mut $name::FnMutRef(ref mut closure) => {
						closure($($t),*)
					},
					
					&mut $name::FnMutBox(ref mut closure) => {
						closure($($t),*)
					},
				}
			}
		}
		
		sized_fn_struct!(_comm $name [ $($t),* ] with $name_method);
		
		
		/**
		 * Create `SizedFnMut` from object and bare method function reference
		 * 
		 * Mutable version of [from_meth](#method.from_meth). Allows the
		 * target object to be mutated when the method is invoked.
		 * 
		 * # Examples
		 * 
		 * See the [Call method that updates object state](index.html#call-method-that-updates-object-state) example.
		 */
		impl<'a, $($t,)* Ret, Type> FromMut<(&'a mut Type, fn(&mut Type, $($t),*) -> Ret)> for $name<'a, $($t,)* Ret> {
			fn from_mut(method_bundle: (&'a mut Type, fn(&mut Type, $($t),*) -> Ret)) -> Self {
				let (object, method) = method_bundle;
				$name::MethodMut($name_method_mut::new(object, method))
			}
		}
		
		
		/**
		 * Create `SizedFnMut` from a mutable boxed closure
		 * 
		 * # Examples
		 * 
		 * See the [Passing a closure that updates values in the current function](index.html#passing-a-closure-that-updates-values-in-the-current-function) example.
		 */
		impl<'a, $($t,)* Ret, Fun> FromMut<&'a mut Fun> for $name<'a, $($t,)* Ret> where Fun: FnMut($($t),*) -> Ret {
			fn from_mut(closure: &'a mut Fun) -> Self {
				$name::FnMutRef(closure)
			}
		}
		
		/**
		 * Create `SizedFnMut` from a mutable boxed closure
		 */
		impl<'a, $($t,)* Ret> FromMut<Box<FnMut($($t,)*) -> Ret>> for $name<'a, $($t,)* Ret> {
			fn from_mut(closure: Box<FnMut($($t,)*) -> Ret>) -> Self {
				$name::FnMutBox(closure)
			}
		}
		
		
		/**
		 * Create `SizedFnMut` from immutable `SizedFn`
		 */
		impl<'a, $($t,)* Z> From<$name_const<'a, $($t,)* Z>> for $name<'a, $($t,)* Z> {
			fn from(other: $name_const<'a, $($t,)* Z>) -> Self {
				match other {
					$name_const::Function(function) => {
						$name::Function(function)
					},
					
					$name_const::Method(method)     => {
						$name::Method(method)
					},
					
					$name_const::FnRef(closure) => {
						$name::FnRef(closure)
					},
					
					$name_const::FnBox(closure) => {
						$name::FnBox(closure)
					},
				}
			}
		}
	);
}

/* Unfortunately we have to generate a struct for every possible parameter count number */

macro_rules! sized_fn {
	( $name:ident + $name_mut:ident [ $($t:ident),* ] with $name_method:ident + $name_method_mut:ident ) => {
		// Generate storage for immutable method reference
		sized_fn_method_struct!(const $name_method[$($t),*]);
		
		// Generate enum for constant references
		sized_fn_struct!(const $name[$($t),*] with $name_method);
		
		// Generate storage for mutable method reference
		sized_fn_method_struct!(mut $name_method_mut[$($t),*]);
		
		// Generate enum for mutable references
		sized_fn_struct!(mut $name_mut[$($t),*] from $name with $name_method + $name_method_mut);
	}
}

sized_fn!(SizedFn0+SizedFnMut0[] with SizedFnMethod0 + SizedFnMutMethod0);
sized_fn!(SizedFn1+SizedFnMut1[A] with SizedFnMethod1 + SizedFnMutMethod1);
sized_fn!(SizedFn2+SizedFnMut2[A, B] with SizedFnMethod2 + SizedFnMutMethod2);
sized_fn!(SizedFn3+SizedFnMut3[A, B, C] with SizedFnMethod3 + SizedFnMutMethod3);
sized_fn!(SizedFn4+SizedFnMut4[A, B, C, D] with SizedFnMethod4 + SizedFnMutMethod4);
sized_fn!(SizedFn5+SizedFnMut5[A, B, C, D, E] with SizedFnMethod5 + SizedFnMutMethod5);
sized_fn!(SizedFn6+SizedFnMut6[A, B, C, D, E, F] with SizedFnMethod6 + SizedFnMutMethod6);
sized_fn!(SizedFn7+SizedFnMut7[A, B, C, D, E, F, G] with SizedFnMethod7 + SizedFnMutMethod7);


#[cfg(test)]
mod tests {
	use ::FromMut;
	
	fn test_func(message: &str) {
		print!("{}", message);
	}
	
	#[test]
	fn call_func() {
		let callback: ::SizedFn1<&str, _> = ::SizedFn1::from(test_func as fn(_));
		
		callback.call("Hallo World");
	}
	
	
	
	struct TestObject {
		value: i32
	}
	
	impl TestObject {
		pub fn new() -> Self {
			TestObject {
				value: 5
			}
		}
		
		pub fn do_calculation(&self, value1: i32, value2: i32) -> i32 {
			(value1 + value2) * self.value
		}
	}
	
	#[test]
	fn call_meth() {
		let object = TestObject::new();
		let callback: ::SizedFn2<i32, i32, i32> = ::SizedFn2::from(
				(&object, TestObject::do_calculation as fn(&_, _, _) -> _)
		);
		
		assert_eq!(callback.call(29, 129), 790);
	}
	
	
	
	#[test]
	fn call_fn_ref() {
		let values = [1, 2, 3];
		let closure = || {
			values.iter().fold(0, |acc, &x| acc + x)
		};
		let callback: ::SizedFn0<i32> = ::SizedFn0::from(&closure);
		
		assert_eq!(callback.call(), 6);
	}
	
	
	
	#[test]
	fn call_fn_box() {
		let closure = |text1: &str, text2: &str, text3: &str| {
			let mut result = String::with_capacity(text1.len() + 1 + text2.len() + 1 + text3.len());
			result.push_str(text1);
			result.push(' ');
			result.push_str(text2);
			result.push(' ');
			result.push_str(text3);
			
			result
		};
		let callback: ::SizedFn3<&str, &str, &str, String> = ::SizedFn3::from(Box::new(closure));
		
		assert_eq!(callback.call("Alle", "meine", "Entchen"), String::from("Alle meine Entchen"));
	}
	
	

	struct Divider {
		divisor: i32,
		result:  String
	}
	
	impl Divider {
		pub fn new(divisor: i32) -> Self {
			Divider {
				divisor: divisor,
				result:  String::new()
			}
		}
		
		pub fn divide_and_format(&mut self, number: i32) {
			self.result = format!("{} + {}/{}",
				(number / self.divisor),
				(number % self.divisor),
				self.divisor
			);
		}
		
		pub fn get_result(&self) -> String {
			self.result.clone()
		}
	}
	
	#[test]
	fn call_mut_method() { 
		let mut divider = Divider::new(5);
		
		{
			let mut callback = ::SizedFnMut1::from_mut(
					(&mut divider, Divider::divide_and_format as fn(&mut Divider, i32)));
			callback.call(29);
		}
		
		assert_eq!(divider.get_result(), String::from("5 + 4/5"));
	}
}
