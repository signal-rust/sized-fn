# sized-fn – Sized function and method pointer storage

This crate provides types for storing function and method pointers in Rust,
without having to fiddle with `Fn()`-references or forcing all of your callers
to use `Box<…>` for storing references to non-closure functions.

For more information, please have a look at
[the documentation](https://ipfs.io/ipns/QmZ86ow1byeyhNRJEatWxGPJKcnQKG7s51MtbHdxxUddTH/Software/Rust/sized-fn/docs/sizedfn/)
(and its example section in particular).


